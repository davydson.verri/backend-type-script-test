# Desafio Mutual - Node TS Developer

Olá, obrigado pelo interesse em fazer parte da nossa equipe de engenharia! 

O objetivo do desafio é entender o quanto você conhece sobre conceitos, planejamento e codificação. [Clique aqui](https://gitlab.com/davydson.verri/bushido) para ver quais são os valores e responsabilidades dos nossos devs.

## Instruções

- Crie um repositório no Github para versionar o seu código
- Crie um board do tipo Kanban(To do, Doing, Done) em alguma ferramenta da sua preferência. Sugestão: você pode usar o [Trello](https://trello.com/)
- Crie todas as tarefas neste board para lhe auxiliar no planejamento e execução deste desafio.
- Utilize Type Script com Node para construir este desafio :rocket:
- Fique à vontade para escolher os frameworks, libs, banco de dados, etc.
- Terminou? [Clique aqui](https://forms.gle/FuK8P8YjMVyXVkSx8) para enviar o seu teste

## Contexto

Uma fintech está iniciando sua operação no Brasil e pretende se posicionar como um banco digital. Após a equipe de produto realizar diversas análises chegaram a seguinte conclusão, vão iniciar a construção da plataforma pelo core bancário que deve possuir as seguintes funcionalidades:

- 1 - Criar conta do cliente
- 2 - Creditar conta
- 3 - Debitar conta
- 4 - Transferir fundos entre as contas internas do banco que deve utilizar os recursos de debitar e creditar criados anteriormente
- 5 - Obter saldo da conta

Esta primeira versão do produto é apenas uma prova de conceito, então não precisamos nos preocupar com autenticação e segurança. 

Desenvolva apenas o backend pois nossa equipe de front vai cuidar do visual e da integração.

## Sua missão

Crie uma API com 5 endpoints para permitir que os front-devs possam implementar as 5 funcionalidades citadas anteriormente.

Segue uma sugestão para te auxiliar na criação das entidades:

**account**
|Field     |Type       |
|-----------|----------:|
|id         |id         |
|cpf        |str(12)    |
|name       |str(50)    |
|created_at |datetime   |

**movement**
|Field      |Type       |
|---------- |----------:|
|id         |id         |
|accountId  |id         |
|value      |int        | - in cents
|created_at |datetime   |

## Requisitos

- Não permitir mais de uma conta por CPF
- Não permitir que as contas fiquem negativas

## O que você não pode esquecer

- Definir corretamente os verbos e rotas da API usando boas práticas REST.
- Utilizar o docker. Para avaliar seu desafio nosso avaliador vai rodar apenas o comando "docker-compose up". Ele deve subir todo o ambiente, incluindo o banco de dados.
- Testar unitariamente de ponta a ponta o fluxo de transferir fundos
- Criar um arquivo README.md na raíz do projeto com instruções sobre como executar e testar o seu projeto
- Criar collection no [Postman](https://www.postman.com/downloads/) para testar o seu projeto. Exportar a collection e incluir na raís do projeto com o nome __postman.json__

## O que será um diferencial

- Criar documentação Swagger, ReDoc... Dê preferência a documentação automática
- Disponibilizar Health Check

## O que será avaliado

### Como você planejou

- Organização: A qualidade do planejamento será refletido na execução. Entenda o problema, crie as tarefas e estime com prazos realistas
- Tamanho das tarefas: Evite criar tarefas com estimativas enormes
- Estimativa: Registre qual é a estimativa em horas para finalizar as tarefas
- Esforço: Registre quantas horas você precisou para finalizar uma tarefa
- Categorização: Indique qual é o tipo de tarefa (codificação, teste, estudo, etc) e saberemos onde você aplicou mais esforço

### Como você desenvolveu

- Flexibilidade: Deve ser fácil incluir novas features, substituir ferramentas...
- Testabilidade: O código deve ser fácil de testar. [Leia mais](https://artesoftware.com.br/2019/02/09/complexidade-ciclomatica/) sobre complexidade ciclomática
- Simplicidade: O código deve ser fácil de entender. [Leia mais](https://artesoftware.com.br/2019/02/10/complexidade-cognitiva/) sobre complexidade cognitiva
- Duplicação de código: Reaproveite o máximo de código para evitar duplicações
- Vulnerabilidades: Evite deixar informações sensíveis no código (senhas, conn strings, etc)
- Bugs: Atenção aos detalhes para não permitir erros inesperados

## Isso pode ajudar

- [DDD](https://www.youtube.com/watch?v=2X9Q97u4tUg&list=PLkpjQs-GfEMN8CHp7tIQqg6JFowrIX9ve)
- [Design Patterns](https://github.com/torokmark/design_patterns_in_typescript)
- [Mongoose](https://mongoosejs.com/)
- [Type ORM](https://typeorm.io/)
- [Jest](https://jestjs.io/pt-BR/)
- [Docker Compose](https://imasters.com.br/banco-de-dados/docker-compose-o-que-e-para-que-serve-o-que-come)
- [Awesome README](https://github.com/matiassingers/awesome-readme)
